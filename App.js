import React, { useEffect } from 'react';
import Routes from './src/screens/routes';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';
import { Alert } from 'react-native';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDYUD6UmShNuJRQZYC02goyGlk-phPOi5E",
  authDomain: "sanbercodelanjutanquizdua.firebaseapp.com",
  databaseURL: "https://sanbercodelanjutanquizdua.firebaseio.com",
  projectId: "sanbercodelanjutanquizdua",
  storageBucket: "sanbercodelanjutanquizdua.appspot.com",
  messagingSenderId: "740348352643",
  appId: "1:740348352643:web:b8f0030abdd79609df23b6",
  measurementId: "G-M0V4RTZEDG"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
}

const App: () => React$Node = () => {

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("6492c977-c38b-4895-be99-14412397d725", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE
    }, SyncStatus)


    return () => {
      OneSignal.addEventListener('received', onReceived);
      OneSignal.addEventListener('opened', onOpened);
      OneSignal.addEventListener('ids', onIds);
    }

  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for Update')
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading Package')
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date')
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing Update')
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        console.log("Notification", "Update Installed")
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting User')
        break;
      default:
        break;
    }
  }


  const onOpened = (notification) => {
    console.log(notification);
  }

  const onReceived = (openResult) => {
    console.log(openResult);
  }

  const onIds = (device) => {
    console.log(device);
  }


  return (
    <Routes />
  );
};

export default App;

// setting icon di android/app/build.grade
// color #18AF02
// 5e:8f:16:06:2e:a3:cd:2c:4a:0d:54:78:76:ba:a6:f3:8c:ab:f6:25
// One Signal 6492c977-c38b-4895-be99-14412397d725
// Codepush P : TNYPJpkQJ8wsuxS3MX6AX6e-fSMqZNegivMQ-M S : WVEjfPFdI7dO2MMmzxoW0SLayJqmuHlU1SEMQ (SanbercodeLanjutanMiniProject)