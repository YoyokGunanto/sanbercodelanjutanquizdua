import { StyleSheet } from 'react-native';
import color from './color';

export default StyleSheet.create({
    containnerLine: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
    },
    lineStyle: {
        flex: 1,
        borderWidth: 1,
        borderColor: color.defaule,
        marginVertical: 30
    },
    space: (value) => {
        return {
            height: value
        };
    }
})