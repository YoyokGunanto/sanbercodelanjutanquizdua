import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: "#ffffff",
    },
    slide: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff",
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#18AF02',
    },
    image: {
        marginVertical: 0
    },
    text: {
        fontSize: 14.5,
        textAlign: "center",
        color: '#949494',
    },
    buttonCircle: {
        flex: 1,
        width: 50,
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 50 / 2,
        backgroundColor: '#18AF02',
    }
})