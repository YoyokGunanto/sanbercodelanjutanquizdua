import React, { useState } from 'react';
import { View, Text, StyleSheet, Dimensions, StatusBar, Image, TextInput, ScrollView } from 'react-native';

import { ButtonDefault, InputText, InputPassword, SparatorOr } from '../../components';
import generalStyle from '../../style/generalStyle';
import color from '../../style/color';
import ProfileItemList from '../../components/ProfileItemList';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const DetailProduct = ({ navigation }) => {

    const [order, setOrder] = useState(1);

    return (
        <View style={styles.containner}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Image source={require('../../assets/images/gacoan.jpg')} style={styles.image} />
                </View>
                <View style={generalStyle.space(15)} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10 }}>
                    <Text style={[{ fontWeight: "bold" }, styles.text]}>Mie Gacoan</Text>
                    <Text style={[styles.text, { color: '#e74c3c' }]}>Rp. 10.000</Text>
                </View>
                <View style={generalStyle.space(5)} />
                <View style={{ paddingHorizontal: 10, flexDirection: 'row' }}>
                    <MCI name="star" size={20} color="#f1c40f" />
                    <MCI name="star" size={20} color="#f1c40f" />
                    <MCI name="star" size={20} color="#f1c40f" />
                    <MCI name="star-half-full" size={20} color="#f1c40f" />
                    <MCI name="star-outline" size={20} color="#f1c40f" />
                </View>
                <View style={{ paddingHorizontal: 20 }}>
                    <View style={generalStyle.space(15)} />
                    <ProfileItemList icon="map-marker-radius-outline" title="Jl. Colombo Samirono" bold="normal" onPress={() => navigation.navigate('Location')} />
                    <ProfileItemList icon="chart-line" title="8.8 (194)" bold="normal" onPress={() => navigation.navigate('Chart')} />
                    <View style={generalStyle.space(15)} />
                    <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: "center" }}>
                        <MCI name="minus-box" size={30} onPress={() => setOrder(order - 1)} />
                        <TextInput
                            style={styles.input}
                            value={order.toString()}
                        // onChangeText={onChange}
                        />
                        <MCI name="plus-box" size={30} onPress={() => setOrder(order + 1)} />
                    </View>
                    <View style={generalStyle.space(10)} />
                    <ButtonDefault title="ORDER" onPress={() => navigation.navigate('Chat')} />
                </View>
            </ScrollView>
        </View >
    );
}

export default DetailProduct;

const styles = StyleSheet.create({
    containner: {
        height: height,
        width: width,
        backgroundColor: color.white,
        padding: width / 40
    },
    image: {
        width: width * 0.90,
        height: height * 0.3,
    },
    text: {
        fontSize: 20,

    },
    input: {
        height: 40,
        width: 100,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 3,
        textAlign: "center",
        fontSize: 15
    }
});