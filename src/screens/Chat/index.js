import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, StatusBar, Image, processColor, ActivityIndicator } from 'react-native';

import color from '../../style/color';

import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';

import { ButtonDefault, InputText, InputPassword, SparatorOr } from '../../components';

const Chat = ({ navigation }) => {

    const [isLoading, setIsLoading] = useState(true);
    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})


    useEffect(() => {
        if (isLoading) {
            setTimeout(() => {
                setIsLoading(false)
                alert('Driver ditemukan');
            }, 3000)
        }

        const user = auth().currentUser;
        setUser(user);
        getData();
        return () => {
            const db = database().ref('messages')
            if (db) {
                db.off()
            }
        }

    }, [isLoading]);

    const getData = () => {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            console.log(snapshot);
            const value = snapshot.val(); // Mengambil data dari firebase
            // console.log(value);
            setMessages(previousMessages => GiftedChat.append(previousMessages, value)); // Menampilkan data dari firebase
        })
    }

    const onSend = ((messages = []) => {
        // console.log(messages);
        for (let i = 0; i < messages.length; i++) {
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            });
        }

    })



    if (isLoading) {
        return (
            <View style={[styles.containerLoad, styles.horizontal]}>
                <ActivityIndicator size="large" color="#00ff00" style={styles.loading} />
            </View>
        );
    }

    return (
        <View style={styles.containner}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <GiftedChat
                messages={messages}
                onSend={messages => onSend(messages)}
                user={{
                    _id: user.uid,
                    name: user.email,
                    avatar: "https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/99/UP1675-CUSA11816_00-AV00000000000012//image?_version=00_09_000&platform=chihiro&w=720&h=720&bg_color=000000&opacity=100"
                }}
            />
            <ButtonDefault title="FINISH ORDER" onPress={() => navigation.navigate('Home')} />
        </View >
    );
}

export default Chat;

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        backgroundColor: color.white,
    },
    containerLoad: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'white'
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
});