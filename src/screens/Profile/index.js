import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator
} from 'react-native';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

import Axios from 'axios';
import Api from '../../api';
import Asyncstorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';

import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';

import { ButtonDefault, ButtonOutlineRed } from '../../components';
import generalStyle from '../../style/generalStyle';
import ProfileItemList from '../../components/ProfileItemList';

function Profile({ navigation }) {

    const [userInfo, setUserInfo] = useState(null);
    const [token, setToken] = useState('');

    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem('token');
                return getVenue(token);
                // console.log(token);
            } catch (err) {
                console.log(err);
            }
        }
        getToken();
        getCurrentUser();
    }, [])

    const getVenue = (token) => {
        setToken(token);
        Axios.get(`${Api}/venues`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log('Gagal', err);
            })
    }

    const getCurrentUser = async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            setUserInfo(userInfo);
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                // user has not signed in yet
                // alert('user has not signed in yet');
            } else {
                // some other error
                // alert('some other error');
            }
        }
    }

    const logoutPress = async () => {
        try {
            if (token) {
                await Asyncstorage.removeItem('token');
                auth().signOut();
            } else {
                await GoogleSignin.revokeAccess();// Meghilangkan akses login
                await GoogleSignin.signOut();// Logout
            }
            setUserInfo(null);
            navigation.reset({
                index: 0,
                routes: [{ name: 'Welcome' }],
            });
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>My Profile</Text>
                <View style={generalStyle.space(20)} />
                <View style={{ flexDirection: 'row' }}>
                    <Image source={userInfo === null ? require('../../assets/images/none_user.png') : { uri: userInfo && userInfo.user && userInfo.user.photo }} style={styles.imageProfile} />
                    <View style={styles.detailProfile}>
                        <Text style={{ fontSize: 18, fontWeight: '700' }}>{userInfo === null ? 'User Name' : userInfo && userInfo.user && userInfo.user.name}</Text>
                        <Text>+6282338109646</Text>
                        <Text>{userInfo === null ? 'useremail@gmail.com' : userInfo && userInfo.user && userInfo.user.email}</Text>
                    </View>
                    <View style={styles.changeProfile}>
                        <TouchableOpacity>
                            <MCI name="account-edit-outline" size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={generalStyle.space(30)} />
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Account</Text>
                <View style={generalStyle.space(10)} />
                <ProfileItemList icon="cart" title="My Order" onPress={() => alert('Dalam Pengerjan')} />
                <ProfileItemList icon="ticket-percent" title="My Vocher" onPress={() => alert('Dalam Pengerjan')} />
                <ProfileItemList icon="label-percent" title="Enter Promo Code" onPress={() => alert('Dalam Pengerjan')} />
                <ProfileItemList icon="help-circle" title="Help" onPress={() => alert('Dalam Pengerjan')} />
                <View style={generalStyle.space(20)} />
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>General</Text>
                <ProfileItemList icon="shield-account" title="Privacy Police" onPress={() => alert('Dalam Pengerjan')} />
                <ProfileItemList icon="star-box" title="About FoodShop" onPress={() => alert('Dalam Pengerjan')} />
                <View style={generalStyle.space(10)} />
                <ButtonOutlineRed title="LOGOUT" onPress={() => { logoutPress() }} />
            </ScrollView>
        </View>
    );
}

export default Profile;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#ffffff',
    },
    imageProfile: { width: 80, height: 80, borderRadius: 80 / 2 },
    detailProfile: { justifyContent: "center", paddingHorizontal: 10 },
    changeProfile: { flex: 1, justifyContent: "center", alignItems: 'flex-end', paddingHorizontal: 10, }
});