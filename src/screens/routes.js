import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import color from '../style/color';

import SplashScreen from './SplashScreen';
import Intro from './Intro';
import Welcome from './Welcome';
import Registration from './Registration';
import Login from './Login';
import Home from './Home';
import Profile from './Profile';
import DetailProduct from './DetailProduct';
import Location from './Location';
import Chart from './Chart';
import Chat from './Chat';

import Asyncstorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabsScreen = () => {
    return (
        <Tabs.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: color.defaule,
            }}
        >
            <Tabs.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                        <MCI name="home" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <MCI name="account" color={color} size={size} />
                    ),
                }}
            />
        </Tabs.Navigator>
    );
}

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={props.token != null && 'Home'}>
        {props.intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />}
        <Stack.Screen name="Welcome" component={Welcome} options={{ headerShown: false }} />
        <Stack.Screen name="Registration" component={Registration} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={TabsScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: true }} />
        <Stack.Screen name="DetailProduct" component={DetailProduct} options={{ headerShown: true, title: 'Detail Product' }} />
        <Stack.Screen name="Location" component={Location} options={{ headerShown: true, title: 'Location' }} />
        <Stack.Screen name="Chart" component={Chart} options={{ headerShown: true, title: 'Grafik Penjualan' }} />
        <Stack.Screen name="Chat" component={Chat} options={{ headerShown: true, title: 'Chat With Driver' }} />
    </Stack.Navigator>
)

function AppNavigation() {
    const [isLoading, setIsLoading] = React.useState(true)
    const [intro, setIntro] = React.useState(null);
    const [token, setToken] = React.useState(null);

    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getStatus() {
            try {
                const skipped = await Asyncstorage.getItem('skipped');
                if (skipped !== null) {
                    // console.log(skipped);
                    setIntro(skipped);
                } else {
                    setIntro(null);
                }
            } catch (err) {
                console.log(err)
            }
        }

        async function getToken() {
            try {
                const token = await Asyncstorage.getItem('token');
                if (token !== null) {
                    setToken(token);
                } else {
                    setToken(null);
                }
            } catch (err) {
                console.log(err)
            }
        }
        getStatus();
        getToken();
    }, [])

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation intro={intro} token={token} />
        </NavigationContainer>
    )
}

export default AppNavigation;