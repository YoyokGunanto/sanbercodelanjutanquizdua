import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    Image,
} from 'react-native';
import { ButtonDefault } from '../../components';
import generalStyle from '../../style/generalStyle';

const Welcome = ({ navigation }) => {

    const registration = () => {
        navigation.navigate('Registration');
    }

    const login = () => {
        navigation.navigate('Login');
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <Image source={require('../../assets/images/Logo.png')} style={styles.logo} />
            <View style={generalStyle.space(60)} />
            <ButtonDefault title="LOGIN" onPress={() => { login() }} />
            <View style={generalStyle.containnerLine}>
                <View style={generalStyle.lineStyle} />
                <Text style={{ paddingHorizontal: 8, color: '#18AF02' }}>OR</Text>
                <View style={generalStyle.lineStyle} />
            </View>
            <ButtonDefault title="REGISTER" onPress={() => { registration() }} />
        </View>
    );
};

export default Welcome;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#ffffff',
        alignItems: "center",
        justifyContent: "center"
    },
    logo: {
        width: 200,
        height: 150,
    }
});