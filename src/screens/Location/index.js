import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, StatusBar, Image } from 'react-native';

import generalStyle from '../../style/generalStyle';
import color from '../../style/color';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoieW95b2tndW5hbnRvIiwiYSI6ImNrZGZwcnN0eTRsMzQyeHF2bHd5aXl4aTcifQ.ugA4GbaOQL-8wDAe7Z_5nw');

const Location = ({ navigation }) => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions();
            } catch (error) {
                console.log(error);
            }
        }
        getLocation();
    }, []);


    return (
        <View style={styles.containner}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <MapboxGL.MapView
                style={{ flex: 1 }}
            >
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />

                <MapboxGL.PointAnnotation
                    id="pointAnnotation"
                    coordinate={[110.327168, -7.695752]}
                >
                    <MapboxGL.Callout
                        title="Mie Gacoan"
                    />
                </MapboxGL.PointAnnotation>

            </MapboxGL.MapView>
        </View >
    );
}

export default Location;

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        backgroundColor: color.white,
    },
});