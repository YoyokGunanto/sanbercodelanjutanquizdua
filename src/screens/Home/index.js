import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    Dimensions
} from 'react-native';

// import Asyncstorage from '@react-native-community/async-storage';
// import Axios from 'axios';
// import Api from '../../api';

import { ButtonDefault, InputText, InputPassword, SparatorOr } from '../../components';
import generalStyle from '../../style/generalStyle';
import color from '../../style/color';

import Ion from 'react-native-vector-icons/Ionicons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

function Home({ navigation }) {

    const [search, setSearch] = useState('');

    const menuItem = (title, caption) => {
        return (
            <View style={styles.thumbnails}>
                <Text style={{ fontSize: 18, fontWeight: "bold" }}>{title}</Text>
                <Text style={{ fontSize: 14 }}>{caption}</Text>
                <View style={generalStyle.space(5)} />
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <TouchableOpacity style={styles.cards} onPress={() => navigation.navigate('DetailProduct')}>
                        <Image source={require('../../assets/images/gacoan.jpg')} style={{ width: 130, height: 100 }} />
                        <View style={generalStyle.space(15)} />
                        <Text style={styles.textPromo}> Mie Gacoan</Text>
                        <Text> Rp 10.000</Text>
                        <View style={generalStyle.space(10)} />
                    </TouchableOpacity>
                    <View style={styles.cards}>
                        <Image source={require('../../assets/images/chachamilk.jpg')} style={{ width: 130, height: 100 }} />
                        <View style={generalStyle.space(15)} />
                        <Text style={styles.textPromo}> ChaChaMilk</Text>
                        <Text> Rp 10.000</Text>
                    </View>
                    <View style={styles.cards}>
                        <Image source={require('../../assets/images/pizza.jpg')} style={{ width: 130, height: 100 }} />
                        <View style={generalStyle.space(15)} />
                        <Text style={styles.textPromo}> Pizza Hut</Text>
                        <Text> Rp 10.000</Text>
                    </View>
                    <View style={styles.cards}>
                        <Image source={require('../../assets/images/heyhey.jpg')} style={{ width: 130, height: 100 }} />
                        <View style={generalStyle.space(15)} />
                        <Text style={styles.textPromo}> HeyHey Boba</Text>
                        <Text> Rp 10.000</Text>
                    </View>
                    <View style={styles.cards}>
                        <Image source={require('../../assets/images/sushi.jpg')} style={{ width: 130, height: 100 }} />
                        <View style={generalStyle.space(15)} />
                        <Text style={styles.textPromo}> Sushi</Text>
                        <Text> Rp 10.000</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <InputText title="Search" placeholder="Search" value={search} onChange={(value) => setSearch(value)} />
            <View style={generalStyle.space(1)} />
            <ScrollView showsVerticalScrollIndicator={false} style={{ paddingHorizontal: 2 }}>
                {menuItem('Top Promo', 'Based promos on around you')}
                {menuItem('Top Rating', 'Based on your past eats')}
                {menuItem('Recomended', 'More menu recommended')}
            </ScrollView>
            {/* <View style={generalStyle.space(70)} /> */}
        </View>
    );
}

export default Home;

const styles = StyleSheet.create({
    container: {
        // height: height,
        // width: width,
        flex: 1,
        backgroundColor: color.white,
        padding: 10
    },
    thumbnails: {
        backgroundColor: color.white,
        paddingVertical: 15,
        paddingHorizontal: 10,
    },
    cards: {
        backgroundColor: color.white,
        padding: 3,
        elevation: 4,
        borderRadius: 3,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 4,

        // margin: 10,
        // flex: 1,
        // backgroundColor: 'transparent',
        // borderColor: 'white',
        // borderWidth: 30,
        // overflow: 'hidden',
        // shadowColor: 'black',
        // shadowRadius: 10,
        // shadowOpacity: 1,
    },
    textPromo: {
        fontSize: 17,
    }
});