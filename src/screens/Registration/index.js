import React, { useState } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Modal
} from 'react-native';
import { ButtonDefault, InputText, InputPassword } from '../../components';
import generalStyle from '../../style/generalStyle';
import Ion from 'react-native-vector-icons/Ionicons';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Feat from 'react-native-vector-icons/Feather';
import color from '../../style/color';

import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage';

import TouchID from 'react-native-touch-id';

const config = {
    title: 'Authentication Required', // Android
    imageColor: '#191970', // Android
    imageErrorColor: 'red', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
}

const Registration = ({ navigation }) => {

    const [form, setForm] = useState({
        name: '',
        email: '',
        no_telp: '',
        alamat: '',
        password: '',
    });
    const [isVisible, setIsVisible] = useState(false);
    const [type, setType] = useState('back');
    const [photo, setPhoto] = useState(null);
    const [flash, setFlash] = useState('flash-off');

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back');
    }

    const toggleFlash = () => {
        setFlash(flash === 'flash-off' ? 'flash' : 'flash-off');
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.takePictureAsync(options);
            // console.log(data);
            setPhoto(data);
            setIsVisible(false);
        }
    }

    const uploadImage = (photo) => {
        if (photo === null) {
            alert('Foto tidak boleh kosog');
        } else {
            const sessionId = new Date().getTime();
            return storage()
                .ref(`images/${sessionId}`)
                .putFile(photo)
                .then((response) => {
                    alert('Success Upload')
                    navigation.navigate('Login');
                })
                .catch((error) => {
                    alert(error)
                })
        }
    }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
            .then(success => {
                alert('Authentication Success');
            })
            .catch(error => {
                alert('Authentication Error');
            })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        ref={ref => {
                            camera = ref;
                        }}
                        type={type}
                    >
                        <View style={styles.containnerFooterCam}>
                            <View style={styles.btnFlipContainer}>
                                <TouchableOpacity style={styles.btnFlip} onPress={() => alert('Setting')}>
                                    <Ion name="settings" size={20} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btnFlipContainer}>
                                <TouchableOpacity style={styles.btnFlip} onPress={() => setIsVisible(false)}>
                                    <MCI name="close" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.round} />
                        <View style={styles.rectangle} />
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Feat name="camera" size={35} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.containnerFooterCam}>
                            <View style={styles.btnFooterCam}>
                                <TouchableOpacity style={styles.btnFlip} onPress={() => alert('Open Folder')}>
                                    <MCI name="folder-open-outline" size={20} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btnFooterCam}>
                                <TouchableOpacity style={styles.btnFlip} onPress={() => toggleFlash()}>
                                    <MCI name={flash} size={20} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btnFooterCam}>
                                <TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
                                    <Ion name="md-camera-reverse-outline" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={generalStyle.space(10)} />
                <View style={styles.containerPicture}>
                    <Text style={{ color: color.defaule, fontWeight: 'bold', fontSize: 20 }}>REGISTRASI</Text>
                    <TouchableOpacity onPress={() => setIsVisible(true)}>
                        <View style={generalStyle.space(20)} />
                        <Image source={photo === null ? require('../../assets/images/none_user.png') : { uri: photo.uri }} style={styles.picture} />
                        <MCI name="camera-outline" size={40} style={styles.camera} />
                    </TouchableOpacity>
                </View>
                <View style={generalStyle.space(20)} />
                <InputText title="Nama Lengkap" placeholder="Nama lengkap" value={form.name} onChange={(value) => setForm(value)} />
                <View style={generalStyle.space(10)} />
                <InputText title="Email" placeholder="Email" value={form.email} onChange={(value) => setForm(value)} />
                <View style={generalStyle.space(10)} />
                <InputText title="Nomor Telepon" placeholder="Nomor telepon" value={form.no_telp} onChange={(value) => setForm(value)} />
                <View style={generalStyle.space(10)} />
                <InputText title="Alamat" placeholder="Alamat" value={form.alamat} onChange={(value) => setForm(value)} />
                <View style={generalStyle.space(10)} />
                <InputPassword title="Password" placeholder="Password" value={form.password} onChange={(value) => setForm(value)} />
                <View style={generalStyle.space(10)} />
                <View style={styles.containerPicture}>
                    <TouchableOpacity onPress={() => signInWithFingerprint()}>
                        <MCI name="fingerprint" size={50} />
                    </TouchableOpacity>
                    <Text>Scane Fingerprint</Text>
                </View>
                <View style={generalStyle.space(20)} />
                <ButtonDefault title="REGISTER" onPress={() => { uploadImage(photo === null ? null : photo.uri) }} />
            </ScrollView>
            {renderCamera()}
        </View>
    );
};

export default Registration;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#ffffff',
        // alignItems: "center",
        // justifyContent: "center"
    },
    containerPicture: {
        justifyContent: "center",
        alignItems: "center",
    },
    picture: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2
    },
    camera: {
        position: 'absolute',
        color: 'white',
        top: 55,
        left: 30
    },
    btnFlipContainer: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    btnFlip: {

    },
    round: {
        width: 120,
        height: 150,
        borderRadius: 120 / 2,
        borderWidth: 1,
        marginTop: 80,
        borderColor: 'white',
        alignSelf: "center"
    },
    rectangle: {
        width: 100,
        height: 80,
        borderWidth: 1,
        marginTop: 50,
        borderColor: 'white',
        alignSelf: "center"
    },
    btnTakeContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignSelf: "center"
    },
    btnTake: {
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
        marginBottom: 20,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
    },
    containnerFooterCam: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btnFooterCam: {
        width: 30,
        height: 30,
        borderRadius: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
});