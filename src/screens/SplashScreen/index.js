import React from 'react';
import { View, Image, StyleSheet } from "react-native";

function SplashScreen() {
    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image source={require('../../assets/images/Logo.png')} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff'
    },
    logoContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: "center"
    }
})

export default SplashScreen;