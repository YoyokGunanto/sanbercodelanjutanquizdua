import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, StatusBar, Image, processColor } from 'react-native';

import { BarChart } from 'react-native-charts-wrapper';

// import generalStyle from '../../style/generalStyle';
import color from '../../style/color';

// import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

const Chart = ({ navigation }) => {

    const [data, setData] = useState(
        [{ y: 100 }, { y: 105 }, { y: 102 }, { y: 110 }, { y: 114 }, { y: 115 }, { y: 150 }, { y: 194 }]
    );

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors: [processColor(color.defaule), processColor(color.darkblue)],
                    // stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    // drawFilled: false,
                    // drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'TOP',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })

    return (
        <View style={styles.containner}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <BarChart
                style={{ flex: 1 }}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                chartDescription={{ text: '' }}
                legend={legend}
                animation={{ durationX: 1000 }}
                drawBarShadow={false}
                drawValueAboveBar={true}
                drawHighlightArrow={true}
                marker={{
                    enabled: true,
                    markerColor: '#000000',
                    textColor: 'white',
                    textSize: 14
                }}
            />
        </View >
    );
}

export default Chart;

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        backgroundColor: color.white,
    },
});