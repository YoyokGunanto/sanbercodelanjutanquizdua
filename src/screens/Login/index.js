import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator
} from 'react-native';

import Axios from 'axios';
import Api from '../../api';
import Asyncstorage from '@react-native-community/async-storage';

import { ButtonDefault, InputText, InputPassword, SparatorOr } from '../../components';
import generalStyle from '../../style/generalStyle';

import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

import TouchID from 'react-native-touch-id';

const config = {
    title: 'Authentication Required', // Android
    imageColor: '#191970', // Android
    imageErrorColor: 'red', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
}

function Login({ navigation }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [token, setToken] = useState('');

    const loginPress = () => {
        setIsLoading(true);
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${Api}/login`, data, {
            timeout: 20000
        }).then((res) => {
            setIsLoading(false);
            saveToken(res.data.token)

            return auth().signInWithEmailAndPassword(email, password)
                .then((res) => {
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Home' }]
                    })
                })
                .catch((error) => {
                    console.log(error)
                })


        }).catch((err) => {
            setIsLoading(false);
            alert('Login Failed');
            setEmail('');
            setPassword('');
        })
    }

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem('token', token)
        } catch (err) {
            console.log(err)
        }
    }

    const signinWithGoogle = async () => {
        try {
            await GoogleSignin.hasPlayServices();

            const { idToken } = await GoogleSignin.signIn()
            // console.log(idToken);
            setToken(idToken);
            saveToken(idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken);
            auth().signInWithCredential(credential);

            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }]
            })

        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        configurGoogleSignin();
        setToken('');
    }, [token]);

    const configurGoogleSignin = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '740348352643-mqu80vpdrpf8j4dmuvgbs35i56pfg0n7.apps.googleusercontent.com'
        });
    }

    const signInWithFingerprint = () => {
        setIsLoading(true);
        TouchID.authenticate('', config)
            .then(success => {
                saveToken('FINGERPRINT');
                navigation.navigate('Home');
            })
            .catch(error => {
                alert('Authentication Error');
            })
    }

    if (isLoading) {
        return (
            <View style={[styles.containerLoad, styles.horizontal]}>
                <ActivityIndicator size="large" color="#00ff00" style={styles.loading} />
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={generalStyle.space(20)} />
                <View style={styles.containerLogo}>
                    <Image source={require('../../assets/images/Logo.png')} style={styles.logo} />
                </View>
                <View style={generalStyle.space(20)} />
                <InputText title="Email" placeholder="Email" value={email} onChange={(value) => setEmail(value)} />
                <View style={generalStyle.space(10)} />
                <InputPassword title="Password" placeholder="Password" value={password} onChange={(value) => setPassword(value)} />
                <View style={generalStyle.space(20)} />
                <ButtonDefault title="LOGIN" onPress={() => { loginPress() }} />
                <SparatorOr />
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginRight: 3 }}>
                        <TouchableOpacity style={styles.btnGoogle} onPress={() => signinWithGoogle()}>
                            <MCI name="google" size={25} style={{ color: 'green' }} />
                            <Text style={{ color: 'green' }}> GOOGLE</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: 3 }}>
                        <TouchableOpacity style={styles.btnFb} onPress={() => alert('Signin With Facebook')}>
                            <MCI name="facebook" size={25} style={{ color: 'blue' }} />
                            <Text style={{ color: 'blue' }} > FACEBOOK</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={generalStyle.space(6)} />
                <TouchableOpacity style={styles.btnFg} onPress={() => signInWithFingerprint()}>
                    <MCI name="fingerprint" size={25} style={{ color: 'orange' }} />
                    <Text style={{ color: 'orange' }} > FINGERPRINT</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#ffffff',
    },
    containerLogo: {
        justifyContent: "center",
        alignItems: "center",
    },
    logo: {
        width: 150,
        height: 120,
    },
    containerLoad: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'white'
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    btnGoogle: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        borderColor: 'green',
        borderWidth: 2,
        paddingVertical: 13,
        borderRadius: 5
    },
    btnFb: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        borderColor: 'blue',
        borderWidth: 2,
        paddingVertical: 13,
        borderRadius: 5
    },
    btnFg: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        borderColor: 'orange',
        borderWidth: 2,
        paddingVertical: 13,
        borderRadius: 5
    }
});