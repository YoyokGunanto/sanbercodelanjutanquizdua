import React from 'react';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';
import styles from '../../style/styleIntro';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';

import Asyncstorage from '@react-native-community/async-storage';

//data yang akan digunakan dalam onboarding
const slides = [
    {
        key: 1,
        title: 'FIND FOOD',
        text: 'Kamu bisa memilih makanan kesukaan yang berada dekat denganmu',
        image: require('../../assets/images/1.png')
    },
    {
        key: 2,
        title: 'ADD ADDRESS',
        text: 'Menandai lokasi dimana kamu berada',
        image: require('../../assets/images/2.png'),
    },
    {
        key: 3,
        title: 'DELIVERY',
        text: 'Makanan yang kamu pesan akan segera kami hantarkan',
        image: require('../../assets/images/3.png'),
    },
];

const Intro = ({ navigation }) => {

    //menampilkan data slides kedalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }

    //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
    const onDone = async () => {
        // navigation.navigate('Welcome')

        try {
            await Asyncstorage.setItem('skipped', 'true');
            navigation.reset({
                index: 0,
                routes: [{ name: 'Welcome' }]
            })
        } catch (err) {
            console.log(err)
        }
    }

    //mengcustom tampilan button done
    const renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    //mengcustom tampilan next button
    const renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <View style={{ flex: 1 }}>
                {/* merender atau menjalankan library react-native-app-intro-slider */}
                <AppIntroSlider
                    data={slides}
                    onDone={onDone}
                    renderItem={renderItem}
                    renderDoneButton={renderDoneButton}
                    renderNextButton={renderNextButton}
                    keyExtractor={(item, index) => index.toString()}
                    activeDotStyle={{ backgroundColor: '#191970' }}
                />
            </View>
        </View>
    )
}

export default Intro