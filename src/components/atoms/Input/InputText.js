import React from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet
} from 'react-native';
import color from '../../../style/color';

const InputText = ({ title, placeholder, value, onChange }) => {
    return (
        <View style={styles.container}>
            {/* <Text>{title}</Text> */}
            <TextInput
                style={styles.input}
                value={value}
                // underlineColorAndroid='#C6C6C6'
                placeholder={placeholder}
                onChangeText={onChange}
            />
        </View>
    )
}

export default InputText;

const styles = StyleSheet.create({
    container: {
        marginBottom: 7
    },
    input: {
        borderWidth: 1,
        borderColor: color.defaule,
        borderRadius: 25,
        paddingVertical: 8,
        paddingHorizontal: 18,
        fontSize: 14,
        color: 'gray',
    }
});