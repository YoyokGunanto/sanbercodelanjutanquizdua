import ButtonDefault from './Button';
import ButtonOutlineRed from './Button/ButtonOutlineRed';
import InputText from './Input/InputText';
import InputPassword from './Input/inputPassword';
import SparatorOr from './SparatorOr';

export { ButtonDefault, ButtonOutlineRed, InputText, InputPassword, SparatorOr };