import React from 'react';
import {
    View,
    Text,
} from 'react-native';
import generalStyle from '../../../style/generalStyle';

function SparatorOr() {
    return (
        <View style={generalStyle.containnerLine}>
            <View style={generalStyle.lineStyle} />
            <Text style={{ paddingHorizontal: 8, color: '#18AF02' }}>OR</Text>
            <View style={generalStyle.lineStyle} />
        </View>
    );
}

export default SparatorOr;