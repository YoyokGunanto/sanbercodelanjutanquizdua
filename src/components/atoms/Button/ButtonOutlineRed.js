import React from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import color from '../../../style/color';

const ButtonOutlineRed = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ButtonOutlineRed;

const styles = StyleSheet.create({
    button: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 13,
        borderRadius: 50,
        borderColor: 'red',
        borderWidth: 1,
    },
    text: {
        color: 'red',
        fontWeight: 'bold'
    }
});