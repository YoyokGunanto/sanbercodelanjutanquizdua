import React from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import color from '../../../style/color';

const ButtonDefault = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ButtonDefault;

const styles = StyleSheet.create({
    button: {
        width: '100%',
        backgroundColor: color.defaule,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 13,
        borderRadius: 5,
    },
    text: {
        color: 'white',
        fontWeight: 'bold'
    }
});