import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

function ProfileItemList({ title, icon, bold = 'bold', onPress = '' }) {
    return (
        <TouchableOpacity style={{ borderBottomColor: '#ecf0f1', borderBottomWidth: 1, paddingVertical: 5 }} onPress={onPress}>
            <View style={{ flexDirection: 'row', alignItems: 'center', padding: 5 }}>
                <MCI name={icon} size={25} />
                <Text style={{ fontWeight: bold, paddingHorizontal: 15 }}>{title}</Text>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                    <MCI name="chevron-right" size={27} />
                </View>
            </View>
        </TouchableOpacity>
    );
}

export default ProfileItemList;